const jwt = require('jsonwebtoken');

// Verificar token función de middleware 
let verifyToken = (req, res, next) => {
    let token = req.get('Authorization');
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            res.status(401).json({
                ok: false,
                err: 'Token inválido'
            })
            return;
        }
        req.usuario = decoded.usuario;
        next();
    });
};


module.exports = {
    verifyToken
}