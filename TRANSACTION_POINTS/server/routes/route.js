const Transaction = require('../model/Transaction');
const { verifyToken } = require('../middlewares/auth');
const express = require('express');
let app = express();

app.get('/total_points', verifyToken, async(req, res) => {
    try {
        let total = await Transaction.sum('points', {
            where: {
                status: 1
            }
        });
        res.json({
            error: false,
            total
        });
    } catch (err) {
        res.status(500).json({
            error: true,
            message: err
        });
    }
})

module.exports = app;