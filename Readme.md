## BackEnd Mis Datos

Cada directorio corresponde al microservicio solicitado. Los microservicios asumen una base de datos mysql corriendo en localhost:3306 con la estructura.sql creada y con los siguientes datos:

**Host**: localhost <br />
**Database name**: leal <br />
**User name**: leal_db <br />
**Password**: Leal-Db 

> **Nota**: Para ejecutar los microservicios es necesario acceder a su directorio correspondiente y ejecutar npm install y luego npm start.

Librerías/paquetes utilizados:
- **bcrypt:** Para encriptación de contraseña
- **express:** Framework desarrollo
- **mysql2:** Driver nodeJs/MySQL
- **sequelize:** ORM MySQL
- **jwt:** JSON Web Token
- **json2xls:** Excel file

> **Nota**: Todos los microservicios de TRANSACTION a excepción del getExcel reciben encabezado de **Authentication** con el token generado en el microservicio de **USER_LOGIN**.