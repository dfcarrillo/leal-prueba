const Sequelize = require('sequelize');
const sequelize = new Sequelize('leal', 'leal_db', 'Leal-Db', {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});
let db = sequelize;

module.exports = db;