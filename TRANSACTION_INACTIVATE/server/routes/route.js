const Transaction = require('../model/Transaction');
const { verifyToken } = require('../middlewares/auth');
const express = require('express');
let app = express();

app.put('/update_transaction/:id', verifyToken, async(req, res) => {
    let id = req.params.id;
    try {
        let transaction = await Transaction.update({ status: 0 }, {
            where: {
                transaction_id: id
            }
        });
        res.json({
            error: false,
            message: 'Se inactivó la transacción exitosamente!'
        });
    } catch (err) {
        res.status(500).json({
            error: true,
            message: 'Error al actualizar la transacción!'
        });
    }
})

module.exports = app;