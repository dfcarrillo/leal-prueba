const Transaction = require('../model/Transaction');
const { verifyToken } = require('../middlewares/auth');
const express = require('express');
let app = express();

app.post('/create_transaction', verifyToken, async(req, res) => {
    let body = req.body;

    if (!body.value || !body.points || !body.user_id) {
        return res.status(500).json({
            error: true,
            message: 'Estructura inválida!'
        });
    }
    try {
        let transaction = await Transaction.create({
            value: body.value,
            points: body.points,
            status: body.status,
            user_id: body.user_id
        });
        res.json({
            error: false,
            transaction
        });
    } catch (err) {
        res.status(500).json({
            error: true,
            message: 'Error al crear transacción!'
        });
    }
})

module.exports = app;