//  Puerto
process.env.PORT = process.env.PORT || 3002;

//  Enviroment
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

//  Fecha vencimiento token //60seg * 60mins
process.env.CADUCIDAD_TOKEN = process.env.CADUCIDAD_TOKEN || '48h';

//  Semilla de token
process.env.SEED = process.env.SEED || 'Este-es-el-seed-desarrollo-leal';