require('./server/config/config');
const bodyParser = require('body-parser');
const db = require('./server/db/db');
const express = require('express');
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Configuracion de rutas
app.use(require('./server/routes/route'));

db.authenticate()
    .then(() => {
        console.log('Conectado a BD!');
        // start server
        app.listen(process.env.PORT, () => {
            console.log('Escuchando puerto: ', process.env.PORT);
        });
    })
    .catch(err => {
        console.error('No se pudo conectar a BD: ', err);
    });