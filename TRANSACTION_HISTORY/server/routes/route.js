const Transaction = require('../model/Transaction');
const { verifyToken } = require('../middlewares/auth');
const express = require('express');
let app = express();

app.get('/transaction_history', verifyToken, async(req, res) => {
    try {
        let transactions = await Transaction.findAll({
            order: [
                ['created_date', 'DESC']
            ]
        });
        res.json({
            error: false,
            transactions
        });
    } catch (err) {
        res.status(500).json({
            error: true,
            message: err
        });
    }
})

module.exports = app;