const User = require('../model/User');
const bcrypt = require('bcrypt');
const express = require('express');
let app = express();

app.post('/register', async(req, res) => {
    let body = req.body;
    if (!body.name || !body.lastname || !body.birth_date || !body.email || !body.password) {
        return res.status(500).json({
            error: true,
            message: 'Estructura inválida!'
        });
    }
    try {
        let user = await User.create({
            name: body.name,
            lastname: body.lastname,
            birth_date: body.birth_date,
            email: body.email,
            password: bcrypt.hashSync(body.password, 10)
        });
        res.json({
            error: false,
            user
        });
    } catch (err) {
        const msg = err.parent.code === 'ER_DUP_ENTRY' ? 'El usuario ya se encuentra registrado!' : 'Error al crear el usuario!';
        res.status(500).json({
            error: true,
            message: msg
        });
    }
})

module.exports = app;