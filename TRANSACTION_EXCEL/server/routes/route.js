const Transaction = require('../model/Transaction');
const { verifyToken } = require('../middlewares/auth');
const express = require('express');
let app = express();

app.get('/excel_transactions', async(req, res) => {
    try {
        let transactions = await Transaction.findAll({
            raw: true,
            order: [
                ['created_date', 'DESC']
            ]
        });
        res.xls('data.xlsx', transactions);
    } catch (err) {
        res.status(500).json({
            error: true,
            message: err
        });
    }
})

module.exports = app;