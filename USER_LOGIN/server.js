require('./server/config/config');
const db = require('./server/db/db');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// Configuracion global de rutas
app.use(require('./server/routes/user'));

db.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
        // start node server
        app.listen(process.env.PORT, () => {
            console.log('Escuchando puerto: ', process.env.PORT);
        });
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });