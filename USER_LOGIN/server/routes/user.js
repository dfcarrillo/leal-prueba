const User = require('../model/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const express = require('express');
let app = express();

app.post('/login', async(req, res) => {
    let body = req.body;
    if (!body.email || !body.password) {
        return res.json({
            error: true,
            message: 'La estructura recibida es inválida!'
        });
    }
    try {
        let user = await User.findOne({
            where: {
                email: body.email
            }
        });
        if (!user) {
            return res.status(400).json({
                error: true,
                message: 'Usuario o contraseña incorrectas (confidencial: El usuario no existe)'
            });
        }
        if (!bcrypt.compareSync(body.password, user.dataValues.password)) {
            return res.status(400).json({
                ok: false,
                message: 'Usuario o contraseña incorrectas (confidencial: Contraseña incorrecta)'
            });
        }
        let token = jwt.sign({ usuario: user }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });
        res.json({
            error: false,
            token
        });
    } catch (err) {
        res.status(500).json({
            error: false,
            message: 'Error en el servidor'
        });
    }
})

module.exports = app;