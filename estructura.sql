CREATE TABLE `USER` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `birth_date` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE(`email`)
);

CREATE TABLE `TRANSACTION` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `value` float(16,9) NOT NULL,
  `points` int(11),
  `status` int(1) DEFAULT 1,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`),
  FOREIGN KEY(`user_id`) REFERENCES `USER`(`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
